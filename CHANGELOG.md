# 1.0.0 (2020-03-25)


### Bug Fixes

* add missing krb5_rcache_dir ([425e809](https://gitlab.com/dreamer-labs/maniac/ansible_role_authentication/commit/425e809))


### Features

* Import from B network ([31f754b](https://gitlab.com/dreamer-labs/maniac/ansible_role_authentication/commit/31f754b))
